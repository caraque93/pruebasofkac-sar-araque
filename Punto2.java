package pruebaSofka;
import java.util.Scanner;
public class Punto2 {

	public static void main(String[] args) {
		Scanner leer = new Scanner(System.in);
		
		//Vbles de entrada
		double pesoEq;
		int sw=1;
		
		//Vbles de salida
		int cont=0;
		double mayPeso = 0, menPeso = 0, sumaPeso = 0, prom, vlrCop = 0, vlrUsd, vlrIndividual = 0;
		System.out.println("Ingrese el peso del bulto en kg");
		pesoEq = leer.nextDouble();
		if(pesoEq <= 500) {
			cont++;
			mayPeso = pesoEq;
			menPeso = pesoEq;
			sumaPeso = sumaPeso + pesoEq;
			if(pesoEq <= 25) {
				vlrIndividual = 0;
			}else if(pesoEq >25 && pesoEq <= 300){
				vlrIndividual = 1500 * pesoEq;
				}
				else if( pesoEq > 300 && pesoEq <= 500) {
					vlrIndividual = 2500 * pesoEq;
				}
			vlrCop = vlrCop + vlrIndividual;
			
		}
		else
			System.out.println("Peso no es válido");
		System.out.println("Para registrar otro bulto oprima 1, para obtener totales oprima 2");
		sw = leer.nextInt();
		while(sw == 1) {
			System.out.println("Ingrese el peso del bulto en kg");
			pesoEq = leer.nextDouble();
			if(pesoEq <= 500) {
				cont++;
				sumaPeso = sumaPeso + pesoEq;
				
				if(pesoEq > mayPeso)
					mayPeso = pesoEq;
				if(pesoEq < menPeso)
					menPeso = pesoEq;
				
				if(pesoEq <= 25) {
					vlrIndividual = 0;
				}else if(pesoEq >25 && pesoEq <= 300){
					vlrIndividual = 1500 * pesoEq;
					}
					else if( pesoEq > 300 && pesoEq <= 500) {
						vlrIndividual = 2500 * pesoEq;
					}
				
				vlrCop = vlrCop + vlrIndividual;
				
				if(sumaPeso > 18000) {
					System.out.println("Peso máximo de 18.000 kg exedido");
					sw = 2;
				}
				
			}
			else
				System.out.println("Peso no permitido");
			System.out.println("Para registrar otro bulto oprima 1, para obtener totales oprima 2");
			sw = leer.nextInt();
		}
		
		prom = sumaPeso / cont;
		vlrUsd = vlrCop / 3500;
		System.out.println("La cantidad de bultos registrados es de: " + cont);
		System.out.println("El peso del bulto más pesado es de: " + mayPeso);
		System.out.println("El peso del bulto más liviano es de: " + menPeso);
		System.out.println("El peso promedio de los bultos es: " + prom);
		System.out.println("El ingreso a la aereolinea en COP es: " + vlrCop + " COP");
		System.out.println("El ingreso a la aereolinea en USD es: " + vlrUsd + " USD");
		

	}

}
